Title: KDE
CSS: /css/main.css

#{include head}

KDE
===

* This will become a table of contents (this text will be scraped).
{:toc}

## Introduction

KDE has been rebranded to mean the community instead of the software
collection containing libraries, a desktop environment and other applications.
In the process this huge collection of software has been split
into three chunks, which follow their own release schedule: KDE Frameworks,
Plasma and KDE Applications.

## Sets

There are three sets matching these release bundles to ease installing
the numerous packages:

- `kde-frameworks`: Libraries (previously known as kdelibs)
- `plasma`: A desktop environment using these libraries
- `kde-applications`: Various applications

To install a minimal working desktop use the `plasma` set:

    # cave resolve plasma

To add further packages you might want to install the full `plasma` set:

    # cave show plasma\*

There's also a `kde` set which pulls in all three previously listed sets,
which gives you all official KDE packages:

    # cave resolve kde\*

For a more fine-grained selection there are even more sets (which are all
included in the kde-applications set):

- kdeaccessibility
- kdeadmin
- kdebindings
- kdeedu
- kdegames
- kdegraphics
- kdemultimedia
- kdenetwork
- kdesdk
- kdeutils

You can easily find out which packages are part of the set by running:

    # cave show <set>\*

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
