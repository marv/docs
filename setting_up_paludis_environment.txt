Without separation:

$ ls -ld ~/.paludis-summer
lrwxrwxrwx 1 arbor arbor 51 Jul  1  2009 /home/arbor/.paludis-summer -> infra-scripts.git/playboy/exherbo-unofficial/config

With separation:

% ls -l ~/.paludis-all
total 16
lrwxrwxrwx 1 bo bo  75 Feb 10  2012 general.conf -> ../scm/exherbo/infra-scripts/playboy/exherbo-unofficial/config/general.conf
lrwxrwxrwx 1 bo bo  74 Feb 10  2012 output.conf -> ../scm/exherbo/infra-scripts/playboy/exherbo-unofficial/config/output.conf
lrwxrwxrwx 1 bo bo  75 Feb 10  2012 repositories -> ../scm/exherbo/infra-scripts/playboy/exherbo-unofficial/config/repositories
-rw-r--r-- 1 bo bo 294 Jun 15  2013 repository_defaults.conf

% cat ~/.paludis-all/repository_defaults.conf
my_dir = /home/bo/scm/all-exherbo
my_repos = ${my_dir}/repositories
my_official_repos = ${my_repos}/official
my_dev_repos = ${my_repos}/dev
my_unofficial_repos = ${my_repos}/unofficial
write_cache = ${my_dir}/caches/metadata
names_cache = ${my_dir}/caches/names
newsdir = /var/empty
format = e

Sync, generate-metadata and index
(generate-metadata is threaded - manage-search-index is not, so it pays to run generate-metadata first)

% type -f csync
csync () {
        local PALUDIS_LOG_DIRECTORY=/home/bo/paludis-logs/paludis CAVE
        CAVE=(cave -E :all)
        export PALUDIS_LOG_DIRECTORY
        $CAVE sync && $CAVE generate-metadata && $CAVE manage-search-index -c ~/paludis-all.index
        if [[ $? -ne 0 ]]; then
                echo "csync failed"
                return 1
        fi
}

